{ lib
, stdenv
, buildPackages
, fetchFromGitHub
, autoreconfHook
, pkg-config
, which
, libmysqlclient
, libaio
, luajit
# For testing:
, testers
, sysbench
}:

stdenv.mkDerivation rec {
  pname = "sysbench";
  version = "1.0.20";

  nativeBuildInputs = [ autoreconfHook pkg-config which ];
  buildInputs = [ libaio ];
  depsBuildBuild = [ buildPackages.stdenv.cc ];

  src = fetchFromGitHub {
    owner = "akopytov";
    repo = pname;
    rev = version;
    sha256 = "1sanvl2a52ff4shj62nw395zzgdgywplqvwip74ky8q7s6qjf5qy";
  };

  enableParallelBuilding = true;

  postPatch = ''
    substituteInPlace third_party/concurrency_kit/ck/configure --replace 'COMPILER=`./.1 2> /dev/null`' 'COMPILER=gcc'
  '';

  preConfigure = ''
    d=./hack-bin
    mkdir $d
    ln -s $(which $PKG_CONFIG) $d/pkg-config
    export PATH=$PATH:$(realpath $d)
  '';

  CK_CONFIGURE_FLAGS = "--platform=arm64";

  CROSS = stdenv.cc.targetPrefix;

  configureFlags = [
    "--without-mysql"
    "--without-luajit"
  ];

  passthru.tests = {
    versionTest = testers.testVersion {
      package = sysbench;
    };
  };

  meta = {
    description = "Modular, cross-platform and multi-threaded benchmark tool";
    longDescription = ''
      sysbench is a scriptable multi-threaded benchmark tool based on LuaJIT.
      It is most frequently used for database benchmarks, but can also be used
      to create arbitrarily complex workloads that do not involve a database
      server.
    '';
    homepage = "https://github.com/akopytov/sysbench";
    downloadPage = "https://github.com/akopytov/sysbench/releases/tag/${version}";
    changelog = "https://github.com/akopytov/sysbench/blob/${version}/ChangeLog";
    license = lib.licenses.gpl2;
    platforms = lib.platforms.unix;
  };
}
